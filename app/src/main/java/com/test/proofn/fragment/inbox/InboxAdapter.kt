package com.test.proofn.fragment.inbox

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.test.proofn.R
import com.test.proofn.data.model.DataItem
import com.test.proofn.ui.detail.DetailMessageActivity
import com.test.proofn.util.DateUtil
import kotlinx.android.synthetic.main.item_inbox.view.*

class InboxAdapter (var list: List<DataItem>?, var context: Context?)
    : RecyclerView.Adapter<InboxAdapter.MyViewHolder>() {

    private lateinit var _context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        _context = context!!
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_inbox, p0, false)
        )
    }

    override fun getItemCount(): Int = list?.size!!


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        Glide.with(context!!).load("https://d8q6v9t02if7z.cloudfront.net"+list?.get(position)?.to?.get(0)?.avatarPathMedium).into(holder.imgProfile)
        holder.txtSubject.text = list?.get(position)?.subject
        holder.txtContent.text = list?.get(position)?.contentPreview
        holder.txtTime.text = list?.get(position)?.sentAt
        holder.txtTime.text = DateUtil().stringTimeAgo(list?.get(position)?.sentAt, _context)

        holder.relContainer.setOnClickListener{
            val detailDevice = Intent(_context, DetailMessageActivity::class.java)
            detailDevice.putExtra("id", list?.get(position)?.id)
            _context.startActivity(detailDevice)
        }
    }


    class MyViewHolder  (view: View) : RecyclerView.ViewHolder(view) {
        val imgProfile = view.img_profile
        val txtSubject = view.txt_subject
        val txtContent  = view.txt_content
        val txtTime = view.txt_time
        val relContainer = view.rel_container
    }
}