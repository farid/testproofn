package com.test.proofn.fragment.inbox


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.lead.danone.base.BaseFragment

import com.test.proofn.R
import com.test.proofn.data.model.DataItem
import kotlinx.android.synthetic.main.fragment_inbox.*

class InboxFragment : BaseFragment<InboxPresenter>(), InboxContract {

    override fun createPresenter(): InboxPresenter = InboxPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_inbox

    override fun onFailed(message: String) {
        Toast.makeText(context!!, message, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessListInbox(list: List<DataItem>?) {
        recyclerView.adapter = InboxAdapter(list, context)
        recyclerView.setUpList()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inbox, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter?.getInbox()
    }


}
