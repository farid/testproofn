package com.test.proofn.fragment.profile

import com.lead.danone.base.BaseView
import com.test.proofn.data.model.profile.ProfileResponse
import com.test.proofn.data.model.profile.UploadResponse

interface ProfileContract : BaseView {
    fun onSuccessProfile(profileResponse: ProfileResponse?)
    fun onSuccessUpload(uploadResponse: UploadResponse?)

}