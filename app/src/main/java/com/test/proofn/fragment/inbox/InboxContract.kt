package com.test.proofn.fragment.inbox

import com.lead.danone.base.BaseView
import com.test.proofn.data.model.DataItem
import com.test.proofn.data.model.LoginResponse

interface InboxContract : BaseView {
    fun onSuccessListInbox(list: List<DataItem>?)

}