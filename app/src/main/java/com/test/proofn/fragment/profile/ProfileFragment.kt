package com.test.proofn.fragment.profile


import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.lead.danone.base.BaseFragment
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker

import com.test.proofn.R
import com.test.proofn.data.model.profile.ProfileResponse
import com.test.proofn.data.model.profile.UploadResponse
import com.test.proofn.util.ImageUtils
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ProfileFragment : BaseFragment<ProfilePresenter>(),ProfileContract {

    private val IMAGE_DIRECTORY_NAME = "proofn"
    private var imageList: ArrayList<Image>? = null
    private var pictureImage: File? = null
    private var photoFile: String? = null

    override fun createPresenter(): ProfilePresenter = ProfilePresenter(this)

    override fun getLayout(): Int = R.layout.fragment_profile

    override fun onFailed(message: String) {

    }

    override fun onSuccessProfile(profileResponse: ProfileResponse?) {
        Glide.with(context!!).load("https://d8q6v9t02if7z.cloudfront.net"+profileResponse?.avatar?.avatarPathMedium)
        txt_value_name.text = profileResponse?.fullName
        txt_value_email.text = profileResponse?.email
    }

    override fun onSuccessUpload(uploadResponse: UploadResponse?) {
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter?.getProfile()

        rel_profile.setOnClickListener {
            ImagePicker.with(this)
                .setStatusBarColor(resources.getString(R.color.colorPrimary))
                .setCameraOnly(false)
                .setMultipleMode(false)
                .setFolderMode(true)
                .setShowCamera(true)
                .setFolderTitle("Albums")
                .setImageTitle("Galleries")
                .setDoneTitle("Done")
                .setLimitMessage("You have reached selection limit")
                .setMaxSize(10)
                .setSavePath("ImagePicker")
                .setSelectedImages(imageList)
                .setAlwaysShowDoneButton(true)
                .setKeepScreenOn(true)
                .start()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            imageList = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES)
            if (imageList != null) {
                val bitmap = ImageUtils.getScaledBitmap((imageList as ArrayList<Image>).get(0).path, 720)
                var out: FileOutputStream? = null
                pictureImage = getOutputMediaFile()

                try {
                    out = FileOutputStream(pictureImage)
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 95, out)
                    out!!.flush()
                } catch (e: IOException) {
                } finally {
                    try {
                        out?.close()
                    } catch (e: IOException) {
                    }

                }
                img_profile.setImageBitmap(bitmap)
                presenter?.uploadProfile(pictureImage!!)
                photoFile = ImageUtils.bitmapToBase64(bitmap)
            }
        }
    }


    private fun getOutputMediaFile(): File? {
        val mediaStorageDir = File(
            Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            IMAGE_DIRECTORY_NAME
        )
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(
                    IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory"
                )
                return null
            }
        }
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.getDefault()
        ).format(Date())
        return File(
            mediaStorageDir.path + File.separator
                    + "IMG_" + timeStamp + ".jpg"
        )
    }

}
