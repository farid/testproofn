package com.test.proofn.fragment.inbox

import com.lead.danone.base.BasePresenter
import com.test.proofn.data.model.InboxListResponse
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber

class InboxPresenter (view : InboxContract) : BasePresenter<InboxContract>() {

    private val compositeDisposable = CompositeDisposable()

    init {
        super.attachView(view)
    }

    fun getInbox() {
        view!!.showProgressDialog()
        compositeDisposable.add(
            repositoryImpl.getInbox()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : ResourceSubscriber<InboxListResponse>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: InboxListResponse?) {
                        view!!.dismissDialog()
                        view?.onSuccessListInbox(t?.data)
                    }

                    override fun onError(t: Throwable?) {
                        view!!.dismissDialog()
                        view!!.onFailed(t?.message!!)
                    }

                })
        )
    }

}