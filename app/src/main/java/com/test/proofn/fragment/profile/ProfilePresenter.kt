package com.test.proofn.fragment.profile

import com.lead.danone.base.BasePresenter
import com.test.proofn.data.model.InboxListResponse
import com.test.proofn.data.model.profile.ProfileResponse
import com.test.proofn.data.model.profile.UploadResponse
import com.test.proofn.fragment.inbox.InboxContract
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ProfilePresenter (view : ProfileContract) : BasePresenter<ProfileContract>() {

    private val compositeDisposable = CompositeDisposable()

    init {
        super.attachView(view)
    }

    fun getProfile() {
        view!!.showProgressDialog()
        compositeDisposable.add(
            repositoryImpl.getProfile()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : ResourceSubscriber<ProfileResponse>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: ProfileResponse?) {
                        view!!.dismissDialog()
                        view?.onSuccessProfile(t)
                    }

                    override fun onError(t: Throwable?) {
                        view!!.dismissDialog()
                        view!!.onFailed(t?.message!!)
                    }

                })
        )
    }

    fun uploadProfile(file : File?) {
        val reqFile = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("avatar", file?.name, reqFile)
        val name = RequestBody.create(MediaType.parse("text/plain"), "photos")

        view!!.showProgressDialog()
        compositeDisposable.add(
            repositoryImpl.uploadAvatar(body, name)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : ResourceSubscriber<UploadResponse>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: UploadResponse?) {
                        view!!.dismissDialog()
                        view?.onSuccessUpload(t)
                    }

                    override fun onError(t: Throwable?) {
                        view!!.dismissDialog()
                        view!!.onFailed(t?.message!!)
                    }

                })
        )
    }

}