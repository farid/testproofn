package com.test.proofn.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream

object ImageUtils {

    fun getScaledBitmap(filePath: String, maxSize: Int): Bitmap {
        val option = BitmapFactory.Options()
        option.inJustDecodeBounds = true

        BitmapFactory.decodeFile(filePath, option)

        val w = option.outWidth
        val h = option.outHeight
        option.inSampleSize = 1

        if (w > maxSize || h > maxSize) {
            option.inSampleSize = Math.min(Math.round(w.toFloat() / maxSize), Math.round(h.toFloat() / maxSize))
        }
        option.inJustDecodeBounds = false

        return BitmapFactory.decodeFile(filePath, option)
    }


    fun bitmapToBase64(bitmap: Bitmap): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 40, byteArrayOutputStream)

        val byteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }
}
