package com.test.proofn.ui.login

import com.lead.danone.base.BaseView
import com.test.proofn.data.model.LoginResponse

interface LoginContract : BaseView {
    fun onSuccess(list: LoginResponse?)

}
