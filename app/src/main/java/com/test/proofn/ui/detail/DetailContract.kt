package com.test.proofn.ui.detail

import com.lead.danone.base.BaseView
import com.test.proofn.data.model.DeleteResponse
import com.test.proofn.data.model.InboxDetailResponse
import com.test.proofn.data.model.LoginResponse

interface DetailContract : BaseView {
    fun onSuccessDetail(success: InboxDetailResponse?)

    fun onSuccessDelete(success: DeleteResponse?)

}