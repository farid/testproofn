package com.test.proofn.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.test.proofn.ui.main.MainActivity
import com.test.proofn.R
import com.test.proofn.data.Preference.Preference
import com.test.proofn.ui.login.LoginActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            if(Preference.getUser() != null){
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else{
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        },3000)

    }
}
