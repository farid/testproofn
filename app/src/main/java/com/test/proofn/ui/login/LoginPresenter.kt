package com.test.proofn.ui.login

import com.lead.danone.base.BasePresenter
import com.test.proofn.data.Preference.Preference
import com.test.proofn.data.model.LoginResponse
import com.test.proofn.data.model.body.BodyLogin
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber

class LoginPresenter (view : LoginContract) : BasePresenter<LoginContract>() {

    private val compositeDisposable = CompositeDisposable()

    init {
        super.attachView(view)
    }

    fun login(bodyLogin: BodyLogin) {
        view!!.showProgressDialog()
        compositeDisposable.add(
            repositoryImpl.login(bodyLogin)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : ResourceSubscriber<LoginResponse>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: LoginResponse?) {
                        view!!.dismissDialog()
                        Preference.saveUser(t)
                        view?.onSuccess(t)
                    }

                    override fun onError(t: Throwable?) {
                        view!!.dismissDialog()
                        view!!.onFailed(t?.message!!)
                    }

                })
        )
    }

}