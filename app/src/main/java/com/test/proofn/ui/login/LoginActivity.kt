package com.test.proofn.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.lead.danone.base.BaseActivity
import com.test.proofn.R
import com.test.proofn.data.model.LoginResponse
import com.test.proofn.data.model.body.BodyLogin
import com.test.proofn.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity<LoginPresenter>(), LoginContract {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {

            var bodyLogin = BodyLogin()
            if (edt_email.text.toString().trim().length>0) {
                bodyLogin.identifier = edt_email.text.toString()
                bodyLogin.password = edt_password.text.toString()

                presenter?.login(bodyLogin)
            }
        }
    }

    override fun createPresenter(): LoginPresenter = LoginPresenter(this)

    override fun onFailed(message: String) {
        Toast.makeText(this@LoginActivity, "Failed", Toast.LENGTH_SHORT).show()
    }

    override fun onSuccess(list: LoginResponse?) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
