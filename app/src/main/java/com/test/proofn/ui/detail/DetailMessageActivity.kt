package com.test.proofn.ui.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.lead.danone.base.BaseActivity
import com.test.proofn.R
import com.test.proofn.data.model.DeleteResponse
import com.test.proofn.data.model.InboxDetailResponse
import android.widget.Toast
import com.bumptech.glide.Glide
import com.test.proofn.util.DateUtil
import kotlinx.android.synthetic.main.activity_detail_message.*


class DetailMessageActivity : BaseActivity<DetailPresenter>(), DetailContract {

    override fun createPresenter(): DetailPresenter = DetailPresenter(this)

    override fun onFailed(message: String) {
        Toast.makeText(this@DetailMessageActivity, "Failed", Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessDetail(success: InboxDetailResponse?) {
        txt_content.text = success?.content
        txt_subject.text = success?.subject
        txt_time.text = DateUtil().stringTimeAgo(success?.sentAt, this)

        Glide.with(this).load("https://d8q6v9t02if7z.cloudfront.net"+success?.to?.get(0)?.avatarPathMedium)
    }

    override fun onSuccessDelete(success: DeleteResponse?) {
    }

    var idd : String  = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_message)

        idd = intent.getStringExtra("id")

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        presenter?.getDetailInbox(idd.toInt())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
            R.id.item3 -> {
                presenter?.deleteInbox(idd.toInt())
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
