package com.test.proofn.ui.detail

import com.lead.danone.base.BasePresenter
import com.test.proofn.data.Preference.Preference
import com.test.proofn.data.model.DeleteResponse
import com.test.proofn.data.model.InboxDetailResponse
import com.test.proofn.data.model.LoginResponse
import com.test.proofn.data.model.body.BodyLogin
import com.test.proofn.ui.login.LoginContract
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber

class DetailPresenter (view : DetailContract) : BasePresenter<DetailContract>() {

    private val compositeDisposable = CompositeDisposable()

    init {
        super.attachView(view)
    }

    fun getDetailInbox(id : Int) {
        view!!.showProgressDialog()
        compositeDisposable.add(
            repositoryImpl.getInboxDetail(id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : ResourceSubscriber<InboxDetailResponse>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: InboxDetailResponse?) {
                        view!!.dismissDialog()
                        view?.onSuccessDetail(t)
                    }

                    override fun onError(t: Throwable?) {
                        view!!.dismissDialog()
                        view!!.onFailed(t?.message!!)
                    }

                })
        )
    }


    fun deleteInbox(id : Int) {
        view!!.showProgressDialog()
        compositeDisposable.add(
            repositoryImpl.deleteInbox(id)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : ResourceSubscriber<DeleteResponse>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: DeleteResponse?) {
                        view!!.dismissDialog()
                        view?.onSuccessDelete(t)
                    }

                    override fun onError(t: Throwable?) {
                        view!!.dismissDialog()
                        view!!.onFailed(t?.message!!)
                    }

                })
        )
    }

}