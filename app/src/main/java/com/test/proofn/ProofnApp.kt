package com.test.proofn

import android.app.Application
import com.orhanobut.hawk.Hawk

class ProofnApp : Application() {

    override fun onCreate() {
        super.onCreate()

        Hawk.init(this).build()


    }

}
