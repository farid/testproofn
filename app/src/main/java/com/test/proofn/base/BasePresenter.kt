package com.lead.danone.base

import com.lead.danone.utils.AppSchedulerProvider
import com.lead.danone.utils.SchedulerProvider
import com.test.proofn.data.remote.ApiServices
import com.test.proofn.data.repository.RepositoryImpl

open class BasePresenter <V>{

    val service = ApiServices.getClient()
    val request = RepositoryImpl(service)
    val scheduler = AppSchedulerProvider()
    val repositoryImpl: RepositoryImpl = RepositoryImpl(service)
    val schedulerProvider: SchedulerProvider = scheduler


    var view: V? = null

    fun attachView(view: V) {
        this.view = view
    }

    fun deattachView() {
        this.view = null
    }
}