package com.lead.danone.base

interface BaseView {

    fun showProgressDialog()
    fun dismissDialog()
    fun onFailed(message : String)

}