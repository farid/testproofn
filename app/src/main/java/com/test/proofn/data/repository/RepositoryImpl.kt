package com.test.proofn.data.repository

import com.test.proofn.data.model.DeleteResponse
import com.test.proofn.data.model.InboxDetailResponse
import com.test.proofn.data.model.InboxListResponse
import com.test.proofn.data.model.body.BodyLogin
import com.test.proofn.data.model.profile.ProfileResponse
import com.test.proofn.data.model.profile.UploadResponse
import com.test.proofn.data.remote.ApiInterface
import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody

class RepositoryImpl (private val apiInterface: ApiInterface) : Repository {

    override fun getProfile(): Flowable<ProfileResponse> = apiInterface.getProfile()

    override fun getInbox(): Flowable<InboxListResponse> = apiInterface.getInbox()

    override fun getInboxDetail(id: Int): Flowable<InboxDetailResponse> = apiInterface.getInboxDetail(id)

    override fun deleteInbox(id: Int): Flowable<DeleteResponse> = apiInterface.deteteInbox(id)

    override fun uploadAvatar(image: MultipartBody.Part, name: RequestBody): Flowable<UploadResponse> = apiInterface.postAvatar(image, name)

    override fun login(bodyLogin: BodyLogin)= apiInterface.postLogin(bodyLogin)

}