package com.test.proofn.data.Preference

import com.orhanobut.hawk.Hawk
import com.test.proofn.data.model.LoginResponse


/**
 * Create by
 * Name    : Lukmanul Hakim
 * on      : 14, February, 2019
 */
class Preference {
    companion object {
        val USER = "USER"

        fun saveUser(loginResponse: LoginResponse?) = Hawk.put(USER, loginResponse)

        fun getUser() : LoginResponse? = Hawk.get(USER)

        fun deleteUser() = Hawk.delete(USER)

    }
}