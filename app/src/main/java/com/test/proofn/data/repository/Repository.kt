package com.test.proofn.data.repository

import com.test.proofn.data.model.DeleteResponse
import com.test.proofn.data.model.InboxDetailResponse
import com.test.proofn.data.model.InboxListResponse
import com.test.proofn.data.model.LoginResponse
import com.test.proofn.data.model.body.BodyLogin
import com.test.proofn.data.model.profile.ProfileResponse
import com.test.proofn.data.model.profile.UploadResponse
import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Part

interface Repository {
    fun login (bodyLogin: BodyLogin) : Flowable<LoginResponse>

    fun getInbox () : Flowable<InboxListResponse>

    fun getInboxDetail (id : Int) : Flowable<InboxDetailResponse>

    fun deleteInbox (id : Int) : Flowable<DeleteResponse>

    fun getProfile () : Flowable<ProfileResponse>

    fun uploadAvatar(@Part image: MultipartBody.Part, @Part("avatar") name: RequestBody) : Flowable<UploadResponse>
}