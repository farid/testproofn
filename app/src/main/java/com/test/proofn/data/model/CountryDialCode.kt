package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName

data class CountryDialCode(

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("dialCode")
	val dialCode: String? = null,

	@field:SerializedName("name")
	val name: String? = null
)