package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName
import com.test.proofn.data.model.DataItem

data class InboxListResponse(

	@field:SerializedName("data")
	val data: List<DataItem>? = null
)