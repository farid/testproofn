package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName

data class InboxDetailResponse(

	@field:SerializedName("bcc")
	val bcc: List<Any?>? = null,

	@field:SerializedName("attachments")
	val attachments: List<Any?>? = null,

	@field:SerializedName("asPart")
	val asPart: Int? = null,

	@field:SerializedName("subject")
	val subject: String? = null,

	@field:SerializedName("isRead")
	val isRead: Boolean? = null,

	@field:SerializedName("externalID")
	val externalID: String? = null,

	@field:SerializedName("box")
	val box: String? = null,

	@field:SerializedName("forwardFrom")
	val forwardFrom: String? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("receivedAt")
	val receivedAt: String? = null,

	@field:SerializedName("subjectPreview")
	val subjectPreview: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("createdAt")
	val createdAt: String? = null,

	@field:SerializedName("lastReadAt")
	val lastReadAt: Any? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("contentType")
	val contentType: String? = null,

	@field:SerializedName("updatedAt")
	val updatedAt: String? = null,

	@field:SerializedName("cc")
	val cc: List<Any?>? = null,

	@field:SerializedName("firstReadAt")
	val firstReadAt: Any? = null,

	@field:SerializedName("inReplyTo")
	val inReplyTo: String? = null,

	@field:SerializedName("sentAt")
	val sentAt: String? = null,

	@field:SerializedName("ownerID")
	val ownerID: String? = null,

	@field:SerializedName("labels")
	val labels: List<Any?>? = null,

	@field:SerializedName("ownerEmail")
	val ownerEmail: String? = null,

	@field:SerializedName("threadID")
	val threadID: String? = null,

	@field:SerializedName("sender")
	val sender: Sender? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null,

	@field:SerializedName("editedAt")
	val editedAt: Any? = null,

	@field:SerializedName("readerList")
	val readerList: List<Any?>? = null,

	@field:SerializedName("contentPreview")
	val contentPreview: String? = null,

	@field:SerializedName("attachmentCount")
	val attachmentCount: Int? = null,

	@field:SerializedName("reportCount")
	val reportCount: Int? = null,

	@field:SerializedName("to")
	val to: List<ToItem?>? = null,

	@field:SerializedName("isGroup")
	val isGroup: Boolean? = null,

	@field:SerializedName("deliveryStatus")
	val deliveryStatus: Int? = null,

	@field:SerializedName("participantHash")
	val participantHash: String? = null
)