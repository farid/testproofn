package com.test.proofn.data.model


data class DeleteResponse(
    val message: String? = null
)