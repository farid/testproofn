package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName

data class User(

	@field:SerializedName("avatarPathLarge")
	val avatarPathLarge: String? = null,

	@field:SerializedName("fullName")
	val fullName: String? = null,

	@field:SerializedName("avatarPathSmall")
	val avatarPathSmall: String? = null,

	@field:SerializedName("countryDialCode")
	val countryDialCode: CountryDialCode? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("phoneNumber")
	val phoneNumber: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("hash")
	val hash: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("hasUsablePassword")
	val hasUsablePassword: Boolean? = null,

	@field:SerializedName("avatarPathMedium")
	val avatarPathMedium: String? = null,

	@field:SerializedName("username")
	val username: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)