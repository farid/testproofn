package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName

data class Log(

	@field:SerializedName("all")
	val all: String? = null
)