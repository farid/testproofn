package com.test.proofn.data.model.body

import com.google.gson.annotations.SerializedName

data class BodyLogin(

    @field:SerializedName("identifier")
    var identifier: String? = null,

    @field:SerializedName("password")
    var password: String? = null
    )