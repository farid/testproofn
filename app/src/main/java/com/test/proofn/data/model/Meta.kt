package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName

data class Meta(

	@field:SerializedName("data")
	val data: Any? = null,

	@field:SerializedName("log")
	val log: Log? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("KV")
	val kV: KV? = null,

	@field:SerializedName("draftType")
	val draftType: String? = null
)