package com.test.proofn.data.remote

import com.test.proofn.data.model.DeleteResponse
import com.test.proofn.data.model.InboxDetailResponse
import com.test.proofn.data.model.InboxListResponse
import com.test.proofn.data.model.LoginResponse
import com.test.proofn.data.model.body.BodyLogin
import com.test.proofn.data.model.profile.ProfileResponse
import com.test.proofn.data.model.profile.UploadResponse
import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface {
    @POST("v1/auth/login")
    fun postLogin(@Body bodyLogin: BodyLogin): Flowable<LoginResponse>

    @GET("v1/messages/inbox")
    fun getInbox(): Flowable<InboxListResponse>

    @GET("v1/messages/inbox/{id}")
    fun getInboxDetail(@Path("id") id : Int): Flowable<InboxDetailResponse>

    @GET("v1/user/profile")
    fun getProfile(): Flowable<ProfileResponse>

    @DELETE("v1/messages/inbox/{id}/trash")
    fun deteteInbox(@Path("id") id : Int): Flowable<DeleteResponse>

    @Multipart
    @POST("v1/user/avatar")
    fun postAvatar(@Part image: MultipartBody.Part, @Part("avatar") name: RequestBody) : Flowable<UploadResponse>
}
