package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName

data class ReaderListItem(

	@field:SerializedName("fullName")
	val fullName: String? = null,

	@field:SerializedName("readAt")
	val readAt: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)