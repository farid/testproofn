package com.test.proofn.data.model.profile

import com.google.gson.annotations.SerializedName

data class Avatar(

	@field:SerializedName("avatarPathLarge")
	val avatarPathLarge: String? = null,

	@field:SerializedName("avatarPathSmall")
	val avatarPathSmall: String? = null,

	@field:SerializedName("avatarPathMedium")
	val avatarPathMedium: String? = null
)