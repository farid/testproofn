package com.test.proofn.data.model.profile

import com.google.gson.annotations.SerializedName

data class ProfileResponse(

	@field:SerializedName("topicsToMention")
	val topicsToMention: String? = null,

	@field:SerializedName("notes")
	val notes: List<Any?>? = null,

	@field:SerializedName("dialCode")
	val dialCode: String? = null,

	@field:SerializedName("jobTitle")
	val jobTitle: String? = null,

	@field:SerializedName("topicsToAvoid")
	val topicsToAvoid: String? = null,

	@field:SerializedName("office")
	val office: String? = null,

	@field:SerializedName("sentInvitation")
	val sentInvitation: Boolean? = null,

	@field:SerializedName("suffix")
	val suffix: String? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("followUpTopic")
	val followUpTopic: String? = null,

	@field:SerializedName("email3")
	val email3: String? = null,

	@field:SerializedName("email2")
	val email2: String? = null,

	@field:SerializedName("companyPhone")
	val companyPhone: String? = null,

	@field:SerializedName("workCity")
	val workCity: String? = null,

	@field:SerializedName("homeCity")
	val homeCity: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("homeAddress")
	val homeAddress: String? = null,

	@field:SerializedName("profession")
	val profession: String? = null,

	@field:SerializedName("linkedUserID")
	val linkedUserID: String? = null,

	@field:SerializedName("homePhone")
	val homePhone: String? = null,

	@field:SerializedName("alternatePhone")
	val alternatePhone: String? = null,

	@field:SerializedName("managersName")
	val managersName: String? = null,

	@field:SerializedName("workAddress")
	val workAddress: String? = null,

	@field:SerializedName("agendaInvitationStatus")
	val agendaInvitationStatus: Int? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("kidsName")
	val kidsName: List<Any?>? = null,

	@field:SerializedName("phoneNumber")
	val phoneNumber: String? = null,

	@field:SerializedName("mobilePhone")
	val mobilePhone: String? = null,

	@field:SerializedName("workZIP")
	val workZIP: String? = null,

	@field:SerializedName("homeProvince")
	val homeProvince: String? = null,

	@field:SerializedName("homeCountry")
	val homeCountry: String? = null,

	@field:SerializedName("birthday")
	val birthday: String? = null,

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("email3Title")
	val email3Title: String? = null,

	@field:SerializedName("gender")
	val gender: Int? = null,

	@field:SerializedName("hasAccount")
	val hasAccount: Boolean? = null,

	@field:SerializedName("workCountry")
	val workCountry: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("spouseType")
	val spouseType: Int? = null,

	@field:SerializedName("department")
	val department: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("inMyContact")
	val inMyContact: Boolean? = null,

	@field:SerializedName("homeZIP")
	val homeZIP: String? = null,

	@field:SerializedName("emailTitle")
	val emailTitle: String? = null,

	@field:SerializedName("fullName")
	val fullName: String? = null,

	@field:SerializedName("workProvince")
	val workProvince: String? = null,

	@field:SerializedName("avatar")
	val avatar: Avatar? = null,

	@field:SerializedName("isVIP")
	val isVIP: Boolean? = null,

	@field:SerializedName("email2Title")
	val email2Title: String? = null,

	@field:SerializedName("referral")
	val referral: String? = null,

	@field:SerializedName("workPhone")
	val workPhone: String? = null,

	@field:SerializedName("middleName")
	val middleName: String? = null,

	@field:SerializedName("spouseName")
	val spouseName: String? = null,

	@field:SerializedName("anniversary")
	val anniversary: String? = null
)