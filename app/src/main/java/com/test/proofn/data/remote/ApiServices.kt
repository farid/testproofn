package com.test.proofn.data.remote

import com.google.gson.GsonBuilder
import com.test.proofn.BuildConfig
import com.test.proofn.data.Preference.Preference
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiServices {

    companion object {
        fun getClient(): ApiInterface {

            val okHttpLoggingInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG)
                okHttpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            else
                okHttpLoggingInterceptor.level = HttpLoggingInterceptor.Level.NONE

            val interceptor = Interceptor{chain ->

                var request = chain.request().newBuilder().build()

                if (Preference.getUser() != null){
                    request = chain.request().newBuilder()
                        .removeHeader("Content-Type")
                        .addHeader( "Content-Type", "application/json")
                        .addHeader("Authorization", "Bearer "+Preference.getUser()!!.token)
                        .build()
                }

                return@Interceptor chain.proceed(request)
            }

            val okHttpClient = OkHttpClient().newBuilder()
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(okHttpLoggingInterceptor)
                .addInterceptor(interceptor)
                .build()

            val gson = GsonBuilder().create()

            val retrofit = retrofit2.Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BuildConfig.BASEURL)
                .build()
            val service: ApiInterface = retrofit.create(
                ApiInterface::class.java)

            return service
        }
    }
}