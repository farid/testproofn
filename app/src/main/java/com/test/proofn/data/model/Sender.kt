package com.test.proofn.data.model

import com.google.gson.annotations.SerializedName

data class Sender(

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("avatarPathLarge")
	val avatarPathLarge: String? = null,

	@field:SerializedName("fullName")
	val fullName: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("avatarPathSmall")
	val avatarPathSmall: String? = null,

	@field:SerializedName("hash")
	val hash: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	@field:SerializedName("avatarPathMedium")
	val avatarPathMedium: String? = null
)